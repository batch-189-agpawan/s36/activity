const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoutes = require('./routes/taskRoutes.js')

// Initialize dotenv for usage
dotenv.config()

// Express Setup
const app = express()
const port = 3001
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Mongoose Setup
mongoose.connect(`mongodb+srv://kidcannabeast:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.u0kf2.mongodb.net/S36-Discussion?retryWrites=true&w=majority`, {
	useNewUrlParser : true,
	useUnifiedTopology : true
})

let db = mongoose.connection
db.on('error', () => console.error('Connection Error'))
db.on('open', () => console.log('Connected to MongoDB!'))


// Routes
app.use('/api/tasks', taskRoutes)



// Listen to port
app.listen(port, () => console.log(`Server is running at port ${port}`))