const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()

router.get('/', (request,response) => {
	TaskController.getAllTasks().then((tasks) => response.send(tasks))
})

// Create task
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((task) => response.send(task))
})


//Get a task
router.get('/:id', (request, response) => {
	TaskController.findTask(request.params.id).then((findTask) => response.send(findTask))
})

//update task
router.put('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((updatedTask) => response.send(updatedTask))
})



module.exports = router