//Create model here

const mongoose = require('mongoose')

// Create Schema
const taskSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		default: 'Pending'
	}
})

// export the model with name Task and structure of taskSchema
module.exports = mongoose.model('Task', taskSchema)